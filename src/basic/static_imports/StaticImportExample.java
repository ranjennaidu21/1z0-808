package basic.static_imports;

//in real programming not good to import too many static constant and method as we
//we will not know it is from which class

//for static import we dont have to specify the class name each time
// we call the method or variable below once imported like this:
import static java.lang.Math.*;
import static java.lang.System.*;
import static basic.static_imports.Config.*;

public class StaticImportExample {
    public static void main(String[] args) {
        //without static import
//        int min = Math.min(5,7);
//        System.out.println("min: "+min);
//        System.out.println("Pi : "+Math.PI);

        //with usage of static import above
        int min = min(5,7);
        out.println("min: "+min);
        out.println("Pi : "+PI);

        //now we can use the static varaible constant and method from Config class here
        printConfig();
        out.println("Config Name: "+NAME);
        out.println("Config Static Count: "+STATIC_COUNT);
    }
}
