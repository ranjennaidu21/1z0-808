package basic.static_imports;

import static java.lang.System.*;

public class Config {
    public static final String NAME = "CONFIG_NAME";
    public static final int STATIC_COUNT = 2;

    public static void printConfig(){
        out.println("Name= " +NAME + " & count= " +STATIC_COUNT);
    }
}
