package basic.class_structure;

import static java.lang.Integer.sum;

public class MainMethod {
    public static void main(String[] args){ //the parameter accept list of Strings
        System.out.println(args.length);

        for(int i=0;i< args.length;i++){
            System.out.println("args ["+i+"]= "+args[i]);
        }

        //if you hover over this method name you can see the java doc declared below for sum
        int totalSum = sum(1,2);
        System.out.println("Sum=" +totalSum);
    }

    //testing on java comment.

//    for single line - press ctrl+/

/*
* for
* multiple line press /* and enter
 */

    //for java doc above method type /** and enter then insert name

    /**
     *
     * @param a operand
     * @param b operand
     * @return sum of both
     */
    public int sumOfMethod(int a,int b){
        return a+b;
    }
}

