package basic.class_structure;

public class Person {
    //variable/ field
    private String firstName;
    private String lastName;
    private int age;

    //methods
    public String getFirstName(){
        return firstName;
    }

    public void setFirstName(String firstName){ //parameter
        this.firstName = firstName;
    }

}
