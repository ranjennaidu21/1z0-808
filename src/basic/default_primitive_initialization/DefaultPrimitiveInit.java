package basic.default_primitive_initialization;

public class DefaultPrimitiveInit {

    static boolean myBoolean;
    static char myChar;
    static byte myByte;
    static short myShort;
    static int myInt;
    static long myLong;
    static double myDouble;
    static float myFloat;

    public static void main(String[] args) {
        float myFloat2;
        //in main method we need to initialize the value to use
        //System.out.println(myFloat2);

        //but for static variable outside of this main method it will be initialized with default values

        //print all the static variable above to see what is the value that initialized
        System.out.println("myBoolean="+myBoolean); //default to false
        System.out.println("myChar="+myChar); //cannot see any value but it is there , empty char

        //all this primitives default to 0
        System.out.println("myByte="+myByte);
        System.out.println("myShort="+myShort);
        System.out.println("myInt="+myInt);
        System.out.println("myLong="+myLong);

        //both of this default to 0.0
        System.out.println("myDouble="+myDouble);
        System.out.println("myFloat="+myFloat);


    }
}
