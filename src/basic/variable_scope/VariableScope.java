package basic.variable_scope;

public class VariableScope {

    //global variable available in whole class
    static int myNewInt = 5;

    public static void main(String[] args) {
        //local variable within this method only
        int myLocalInt=10;
        System.out.println("myNewInt="+myNewInt);
        {
            //bracket variable scope
            int myOtherInt=20;
            {
                //cannot initialize same variable name of myOtherInt as it still inside the previous bracket scope
                //int myOtherInt = 30;
                int myOtherInt2 =30;
                System.out.println("myOtherInt inside first bracket of bracket="+myOtherInt);
                System.out.println("myOtherInt2 inside first bracket of bracket="+myOtherInt2);
            }
            {
                System.out.println("myOtherInt inside second bracket of bracket="+myOtherInt);
                //cannot print myOtherInt2 as it is for the above bracket only
                //System.out.println("myOtherInt2 inside second bracket of bracket="+myOtherInt2);

            }


            //myLocalInt will still be printed as this bracket is still inside main method that have the local myLocalInt
            System.out.println("myLocalInt inside bracket scope="+myLocalInt);
        }

        //myOtherInt is only available within the bracket scope , outside that bracket it is unavailable and wont print
        //System.out.println("myOtherInt="+myOtherInt);
        System.out.println("myLocalInt outside bracket scope="+myLocalInt);
        printMyInt();
    }

    public static void printMyInt(){
        //this will not print as myLocalInt only within the main bracket scope
        //System.out.println("myLocalInt="+myLocalInt);

        //but global variable will be printed as same under same class
        System.out.println("myNewInt inside printMyInt()="+myNewInt);
    }

}
