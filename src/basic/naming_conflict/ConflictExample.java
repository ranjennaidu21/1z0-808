package basic.naming_conflict;

//if press ctrl+o will remove unused import , if press ctrl+l will format the code

//this will make both code below work , the Date refer to util.Date
import java.sql.*;
import java.util.Date;

//this will give compilation error to Date not sure refer to which package
//import java.sql.*;
//import java.util.*;

//the second import is redundant as it already assume and take the first one. so Date is referring
//to java.sql.Date
//import java.sql.Date;
//import java.util.Date;

public class ConflictExample {
    Date date;
    //in order to use same class name from different package need to put the full class with package name
    java.sql.Date sqlDate;

    //ignoring all the above it's always recommended to put full name with package if using same class name'
    java.util.Date date2;
    java.sql.Date sqlDate2;
}
