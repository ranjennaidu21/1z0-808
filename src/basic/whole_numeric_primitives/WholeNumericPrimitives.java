package basic.whole_numeric_primitives;
import static java.lang.System.*;

public class WholeNumericPrimitives {
    public static void main(String[] args) {
        //if put too many number even using long by default java compile as integer ,
        //so getting compilation error to big. to fix put big L behind
        long num1 = 123123123123L;

        //from java 7  we can use _ for long numbers , avoid in front,end and after decimal
        long num3 = 32_223_298_322L;
        out.println(num1);
        out.println(num3);

        // octal (0-7)
        int oct = 07;
        int firstOct = 010; // 8 decimal
        int secondOct = 022; // 18 decimal

        int sumOct = firstOct + secondOct; // 26 decimal, 32 octal
        System.out.println("firstOct= " + firstOct + " secondOct= " + secondOct);
        System.out.println("decimal sum= " + sumOct + " octSum= " + Integer.toOctalString(sumOct));

        // hexadecimal (0-9 and A-F)
        int firstHex = 0xF; // 15 decimal
        int secondHex = 0x1E; // 30 decimal
        int sumHex = firstHex + secondHex; // 45 decimal, 2d hex
        System.out.println("firstHex= " + firstHex + " secondHex= " + secondHex);
        System.out.println("decimalSum= " + sumHex + " hexSum= " + Integer.toHexString(sumHex));

        // binary
        int firstBin = 0b1001; // 9 decimal
        int secondBin = 0b0111; // 7 decimal
        int sumBin = firstBin + secondBin; // 16 decimal, 10000 binary

        // int thirdBin = 0b2;
        System.out.println("firstBin= " + firstBin + " secondBin= " + secondBin);
        System.out.println("decimalSum= " + sumBin + " binSum= " + Integer.toBinaryString(sumBin));
    }
}
