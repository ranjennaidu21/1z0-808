package basic.package_imports;

//import java.util.Random;

//you also can use star to include all the classes under util packages
import java.util.*;

public class ImportExample {
    public static void main(String[] args) {
        //as you can see the Random file import is added automatically above once type and enter
        Random random = new Random();
        //bound to 5 , means give random number from 0-5
        System.out.println(random.nextInt(5));
        //java.lang is automatically added so dont have to be imported.
        //for example this System and Intege under java.lang
    }


}
