package basic.primitive_char_boolean;

public class PrimitiveCharAndBoolean {
    public static void main(String[] args) {
        char ch = 'a';

        //char only accept one string value, will give to many characters compilation error
        //char ch1 = 'ab';
        char ch1 = '1';

        //for unicode
        char uniChar = '\u03A9'; // upper case greek omega character
        char romanNumber = '\u216C'; // roman 50 number

        System.out.println("ch1= " + ch1);
        System.out.println("uniChar= " + uniChar);
        System.out.println("romanNumber= " + romanNumber);

        boolean myBoolean = true;
        boolean myFalseBoolean = false;
        //cannot put true or false as variable name as it is java reserve word
        //boolean true = true;

        System.out.println("myBoolean= " + myBoolean);
        System.out.println("myFalseBoolean= " + myFalseBoolean);

        boolean true1 = false;
    }
}
