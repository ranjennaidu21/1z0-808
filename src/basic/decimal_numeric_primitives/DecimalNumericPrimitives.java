package basic.decimal_numeric_primitives;
import static java.lang.System.*;
public class DecimalNumericPrimitives {
    public static void main(String[] args) {

        //same for decimal number java intepret as double by default , so put F behind
        float num2 = 23.123123F;

        //        double before = 10_.25; // does not compile
        //        double after = 10._25; // does not compile
        //        double first = _10.25; // does not compile
        //        double last = 10.25_; // does not compile

        //can be auto converted from float to double
        double db1 = 12.56f;

        //all this three is same
        double db2 = 501.2322e2;
        double db3 = 501.2322e02;
        double db4 = 50123.22;
        out.println(db2);
        out.println(db3);
        out.println(db4);

        double hexPi = 0x1.91eb851eb851fp1; // p indicates hexadecimal floating point number
        System.out.println("hexPi= " + hexPi);
    }
}
