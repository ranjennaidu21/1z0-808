package basic.declarating_initializing_variables;

public class DeclaringIntializingVariables {
    public static void main(String[] args) {
        // declaring and initializing in two lines
        int myNumber; // declaration
        //need to intialize the variable before use it
        //System.out.println("myNumber= " + myNumber);
        myNumber = 10; // initialization
        System.out.println("myNumber= " + myNumber);

        // declare and initialize the variable in one line
        double myDouble = 7.50;
        System.out.println("myDouble= " + myDouble);

        //all this is possible
        float myFloat1, myFloat2, myFloat3;
        float myFloat4;
        float myFloat5;
        float myFloat6 = 5f, myFloat7 = 10f, myFloat8;
        boolean b1, b2;

//        double d1, double d2; // does not compile ,after coma expect variable name

        int i1;
        int i2;
        // int i3; i4; // does not compile as need to give new statement after semicolon

//        int int = 10; // does not compile as int is reserved keyword
        char cHaR;
        char Char;

        //identifier is another name for variable
        //identifier accept $ and _
        float okFloat;
        double $Ok2Double1;
        double _alsoDouble;
        float __OkButNotNice$_123;

        // illegal examples
        // double 3point; //cannot start with number
        // double my@street; //@ is not accepted , can use $ or _
//        float *$coffee; //* is not accepted
//        float double; //double is reserver word
    }
}
